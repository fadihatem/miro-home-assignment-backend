<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrafficTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('traffic', function (Blueprint $table) {
            $table->id();
            $table->string("source");
            $table->bigInteger("users");
            $table->bigInteger("new_users");
            $table->bigInteger("sessions");
            $table->double("bounce_rate");
            $table->dateTime("first_seen_on");
            $table->double("page_session");
            $table->time("session_duration");
            $table->double("conversion_rate");
            $table->double("goal_value");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('traffic');
    }
}
