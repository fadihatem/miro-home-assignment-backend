# Miro Test Exercise


## How to Run Application
```
docker-compose up -d
```
Set up should take up to 10 min depending on Internet Speed to download reqired docker images.

Make sure to clone the frontend application as well next to this repo.

File structure should be :
```
->Parent Folder:
        ->frontend
        ->api
```
