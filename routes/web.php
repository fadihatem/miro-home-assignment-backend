<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET,HEAD,PUT,POST,DELETE,PATCH,OPTIONS");
header("Access-Control-Allow-Headers: Content-Type, X-Auth-Token, Origin, Authorization");
use Illuminate\Support\Facades\Route;

$router->get('/', function () use ($router) {
    return $router->app->version();
});
$router->group(['prefix' => 'api'], function () use ($router) {
    $router->get('/traffic', 'TrafficController@index');
    $router->post('/traffic', 'TrafficController@store');
    $router->get('/traffic/{traffic_id}', 'TrafficController@show');
    $router->put('/traffic/{traffic_id}', 'TrafficController@update');
    $router->delete('/traffic/{traffic_id}', 'TrafficController@destroy');
});

Route::options(
    '/{any:.*}', 
    [ 
        function (){ 
            return response(['status' => 'success']); 
        }
    ]
);
