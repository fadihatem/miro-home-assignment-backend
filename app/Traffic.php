<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Traffic extends Model
{
    protected $table = 'traffic';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "source", "users", "new_users", 
        "sessions", "bounce_rate", "first_seen_on", 
        "page_session", "session_duration", "conversion_rate", "goal_value"
    ];
    public $sortable = ['source',
                        'users',
                        "page_session",
                        'session_duration',
                        'goal_value'];
                        
    protected $hidden = ['created_at', 'updated_at'];

    public function getBounceRateAttribute($value)
    {
        return $value . "%";
    }

    public function getConversionRateAttribute($value)
    {
        return $value . "%";
    }
}
