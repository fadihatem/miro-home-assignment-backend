<?php

namespace App\Http\Controllers;

use App\Traffic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TrafficController extends Controller
{

    public function __construct()
    {
    }

    public function index(Request $request)
    {
        if ($request->has('rows')) {
            $rows = (int) $request->get('rows');
        } else {
            $rows = 20;
        }

        $traffic = Traffic::query();

        if ($request->has('q')) {
            $qSearch = $request->get('q');
            $traffic->where('source', 'LIKE', "%" . $qSearch . "%");
            $traffic->orderBy('id', 'DESC');
        }

        return $this->success($traffic->paginate($rows), 200);
    }

    public function store(Request $request)
    {
        $rules = [
            'file' => 'required',
            'file.*' => 'mimes:csv,txt'
        ];
        $this->validate($request, $rules);
        $files = $request->allFiles('file')['file'];
        foreach ($files as $file) {
            try {
                $path = $file->getRealPath();
                $data = array_map('str_getcsv', file($path));
                if (count($data) === 0) {
                    return $this->error("The Uploaded CSV is empty!", 400);
                }
                array_shift($data);
                $db_data = [];
                foreach ($data as $record) {
                    array_push($db_data, [
                        "source" => $record[0],
                        "users" => intval($record[1]),
                        "new_users" => intval($record[2]),
                        "sessions" => intval($record[3]),
                        "bounce_rate" => floatval($record[4]),
                        "first_seen_on" => $record[5],
                        "page_session" => floatval($record[6]),
                        "session_duration" => $record[7],
                        "conversion_rate" => floatval($record[8]),
                        "goal_value" => floatval($record[9]),
                    ]);
                }

                $traffic = Traffic::insert($db_data);
            } catch (\Exception $e) {
                return $this->error("Oops! You uploaded a CSV with a wrong structure :O.", 400);
            }
        }

        return $this->success("The CSV File(s) where imported successfully", 200);
    }

    public function show($traffic_id)
    {
        $traffic = Traffic::find($traffic_id);

        if (!$traffic) {
            return $this->error("The record with {$traffic_id} doesn't exist", 404);
        }

        return $this->success($traffic, 200);
    }

    public function update(Request $request, $traffic_id)
    {
        $traffic = Traffic::find($traffic_id);
        if (!$traffic) {
            return $this->error("The record with {$traffic_id} doesn't exist", 404);
        }
        $rules = [
            'goal_value' => 'required|numeric'
        ];
        $this->validate($request, $rules);
        $traffic->goal_value = $request->get("goal_value");
        $traffic->save();

        return $this->success($traffic, 200);
    }

    public function destroy($traffic_id)
    {
        $traffic = Traffic::find($traffic_id);

        if (!$traffic) {
            return $this->error("The record with id {$traffic_id} doesn't exist", 404);
        }

        $traffic->delete();

        return $this->success("The record with id {$traffic_id} has been deleted", 200);
    }
}
