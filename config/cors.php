<?php

return [
    "allow_origin" => '*',
    "allow_methods" => 'OPTIONS, DELETE, POST, GET, PUT',
    "allow_credentials" => true,
    "max_age" => '86400',
    "allow_headers" => 'Content-Type, Authorization, X-Requested-With',
];